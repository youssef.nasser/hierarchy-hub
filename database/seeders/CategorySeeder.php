<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Category::factory(5)->create();
        $rootCategories = Category::where('parent_id', null)->get();
        foreach ($rootCategories as $rootCategory) {
            $this->seedCategories($rootCategory, rand(1, 4));
        }
    }

    /**
     * Recursive category seeding with specified depth.
     *
     * Creates child categories for the given parent category up to the specified depth.
     *
     * @param  \App\Models\Category  $parentCategory
     * @param  int  $depth
     * @return void
     */
    private function seedCategories(Category $parentCategory, $depth)
    {
        if ($depth === 0) {
            return;
        }

        $childCategories = Category::factory(rand(1, 3))->create([
            'parent_id' => $parentCategory->id,
        ]);

        foreach ($childCategories as $childCategory) {
            $this->seedCategories($childCategory, $depth - 1);
        }
    }
}
