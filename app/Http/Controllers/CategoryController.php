<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display the specified category and its descendants.
     *
     * @param  int  $categoryId
     * @return \Illuminate\Contracts\View\View
     */
    public function show($categoryId)
    {
        $category = Category::with('descendants')->find($categoryId);

        return view('categories.show', compact('category'));
    }
}
