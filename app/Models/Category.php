<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    use HasFactory;

    /**
     * Fillable attributes for the Category model.
     *
     * @var array
     */
    protected $fillable = [
    'name',
    'parent_id',
    ];

    /**
     * Get child categories associated with the current category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children() : HasMany
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    /**
     * Get the parent category for the current category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent() : BelongsTo
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    /**
     * Get all descendants of the current category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function descendants() : HasMany
    {
        return $this->children()->with('descendants');
    }
}
