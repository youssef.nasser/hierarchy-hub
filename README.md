# Hierarchical Categories Database

## Task

Create a hierarchical database table for categories and subcategories with unlimited tree levels.

## Solution

### Migration

- Run `php artisan migrate` to create "categories" table with `id`, `name`, `parent_id`, and `timestamps`.

### Eloquent Model

- Define `Category` model with relationships: `children()`, `parent()`, and `descendants()`.

### Controller

- Use `CategoryController` to show a category and its descendants with `descendants` relationship.

### View

- Display hierarchically using indentation in `categories.show.blade.php`.
